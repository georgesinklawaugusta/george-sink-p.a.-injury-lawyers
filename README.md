We are a personal injury law firm with locations all across South Carolina and Georgia. We are proud to represent car accident and injured victims who are not receiving their due compensation from the big insurance companies. George Sink and his team of attorneys will fight for you.

Address: 3523 Walton Way Ext, Augusta, GA 30909, USA

Phone: 706-739-6100
